# FROM php:7.4.33
FROM php:7.4.33-apache


# Install otras dependencias
RUN apt-get update && \
    apt-get install -y git zip unzip curl wget


# Install GD extension and Git
RUN apt-get update && \
    apt-get install -y libpng-dev && \
    docker-php-ext-install gd && \
    docker-php-ext-install pdo_mysql && \
    docker-php-ext-install pdo


# INstalar composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === 'e21205b207c3ff031906575712edab6f13eb0b361f2085f1f1237b7126d785e826a450292b6cfd1d64d92e6563bbde02') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer

# Limpieza del sistema:
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/*


WORKDIR /var/www/html

EXPOSE 8000

CMD [ "bash" ]